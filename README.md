# CVEr

Do stuff with CVEs, for less CVEs


## Subcommands

### `imageVulns`

- Reads all issues that look like image vulnerability reports
- Checks the _newest_ version of the reported images against all reported CVEs
- Tells you, which of those issues
  - can be closed, because the images aren't vulnerable to those CVEs anymore
  - which issues are still relevant and you need to look into

## State

super experimental

## TODOs

- [ ] Tests

- [ ] Add other / more image scanners:

  For now, we only shell out to grype. Other scanners could/should be implemented (trivy, twistlock, ...) or a scanner that collects findings from multiple scanners.
  We could also think about not shelling out, but using e.g. grype as a golang library to do the scanning.

- [ ] More / better output:

  E.g. also print the severity / package of the flagged CVE

- [ ] work off based on the project name (ie. gitlab-org/gitlab-runner) instead of the project ID

- [ ] Better ImageRefUpdater:

  Right now, when we see an issue for an image, we actually scan the "bleeding"
  version of the image. The component which changes / updates the image
  reference is right now super stupid, it just works off of a map of well known
  image tags.

  We could:
  - introduce a config file with those mappings
  - have the ImageRefUpdater actually pull the logs from CI, and figure out
    what the newest version of an image is

- [ ] More subcommands for other CVE things
