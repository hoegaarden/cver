package nested_bucket_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestNestedBucket(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "NestedBucket Suite")
}
