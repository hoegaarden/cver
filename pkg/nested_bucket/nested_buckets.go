package nested_bucket

import (
	"errors"
)

// Bucket is a container for grouping things, based on 2 levels of grouping keys
type Bucket[T1, T2 comparable, T3 any] map[T1]map[T2][]T3

// Add will add items to the bucket in the group v1/v2.
func (b Bucket[T1, T2, T3]) Add(v1 T1, v2 T2, v3s ...T3) {
	if _, ok := b[v1]; !ok {
		b[v1] = map[T2][]T3{}
	}

	b[v1][v2] = append(b[v1][v2], v3s...)
}

// Map maps through all items in the Bucket and by that produces a new bucket.
// The new bucket will have the very same "groups" but the items will be the
// result of the mapper function.
func Map[T1, T2 comparable, Torg, Tnew any](orgBucket Bucket[T1, T2, Torg], mapper func(Torg) Tnew) Bucket[T1, T2, Tnew] {
	newBucket := Bucket[T1, T2, Tnew]{}

	_ = Each(orgBucket, func(v1 T1, v2 T2, v3 Torg, i int) error {
		newBucket.Add(v1, v2, mapper(v3))
		return nil
	})

	return newBucket
}

var ErrAbort = errors.New("abort")

// Each calls the function forItem for each item in the [Bucket], with the
// "outer bucket key", "the inner bucket key", the item value, and the index of
// the item inside the inner bucket.
// When forItem returns an error, Each is aborted and this error is returned.
// When forItem returns the special [ErrAbort] error, Each is aborted too, but
// no error is returned.
func Each[T1, T2 comparable, T3 any](bucket Bucket[T1, T2, T3], forItem func(T1, T2, T3, int) error) error {
	for v1, v2s := range bucket {
		for v2, v3s := range v2s {
			for i, v3 := range v3s {
				err := forItem(v1, v2, v3, i)
				if err == ErrAbort {
					return nil
				}
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}
