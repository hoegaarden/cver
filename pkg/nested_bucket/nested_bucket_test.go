package nested_bucket_test

import (
	"fmt"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/hoegaarden/cver/pkg/nested_bucket"
)

var _ = Describe("NestedBucket", func() {
	var (
		bucket nested_bucket.Bucket[int, float32, string]
	)
	BeforeEach(func() {
		bucket = nested_bucket.Bucket[int, float32, string]{}
	})

	Describe("bucket.Add", func() {
		BeforeEach(func() {
			bucket.Add(1, 2.0, "three", "four")
		})
		It("holds the value now", func() {
			Expect(bucket[1][2.0]).To(Equal([]string{"three", "four"}))
		})

		When("we add the same value to the same bucket again", func() {
			BeforeEach(func() {
				bucket.Add(1, 2.0, "three")
			})
			It("appends it", func() {
				Expect(bucket[1][2.0]).To(Equal([]string{"three", "four", "three"}))

			})
		})
	})

	Describe("Each", func() {
		BeforeEach(func() {
			bucket.Add(1, 1.0, "one_1", "one_2")
			bucket.Add(2, 2.0, "two_1", "two_2", "two_3")
		})
		It("call the fun for each item", func() {
			buf := []string{}
			err := nested_bucket.Each(bucket, func(v1 int, v2 float32, item string, idx int) error {
				buf = append(buf, fmt.Sprintf("%d :: %.4f :: %d :: %s", v1, v2, idx, item))
				return nil
			})

			Expect(err).NotTo(HaveOccurred())
			Expect(len(buf)).To(Equal(5))
			Expect(buf).To(ContainElements(
				"1 :: 1.0000 :: 0 :: one_1",
				"1 :: 1.0000 :: 1 :: one_2",
				"2 :: 2.0000 :: 0 :: two_1",
				"2 :: 2.0000 :: 1 :: two_2",
				"2 :: 2.0000 :: 2 :: two_3",
			))
		})

		When("the function returns an error", func() {
			When("the error is ErrAbort", func() {
				It("aborts gracefully and only iterates until error occurs", func() {
					callCount := 0
					err := nested_bucket.Each(bucket, func(v1 int, v2 float32, item string, idx int) error {
						callCount += 1
						return nested_bucket.ErrAbort
					})

					Expect(err).NotTo(HaveOccurred())
					Expect(callCount).To(Equal(1))
				})
			})
			When("the error is some random error", func() {
				It("bubbles this error up", func() {
					callCount := 0
					err := nested_bucket.Each(bucket, func(v1 int, v2 float32, item string, idx int) error {
						callCount += 1
						return fmt.Errorf("this-is-a-real-error")
					})

					Expect(err).To(MatchError("this-is-a-real-error"))
					Expect(callCount).To(Equal(1))
				})
			})
		})
	})

	Describe("Map", func() {
		BeforeEach(func() {
			bucket.Add(1, 1.0, "1")
			bucket.Add(1, 1.1, "22")
			bucket.Add(2, 2.0, "333")
			bucket.Add(2, 2.1, "4444", "55555")
			bucket.Add(2, 2.2, "666666")
		})
		It("returns a new bucket with the values mapped", func() {
			res := nested_bucket.Map(bucket, func(s string) int {
				return len(s)
			})

			// to check, that we have actually a bucket of different type now
			Expect(fmt.Sprintf("%T", res)).To(Equal("nested_bucket.Bucket[int,float32,int]"))
			Expect(res).To(BeEquivalentTo(map[int]map[float32][]int{
				1: {
					1.0: []int{1},
					1.1: []int{2},
				},
				2: {
					2.0: []int{3},
					2.1: []int{4, 5},
					2.2: []int{6},
				},
			}))
		})
	})
})
