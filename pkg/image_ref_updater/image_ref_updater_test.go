package image_ref_updater_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/hoegaarden/cver/pkg/image_ref_updater"
	"gitlab.com/hoegaarden/cver/pkg/testing"
)

var _ = Describe("ImageRefUpdater", func() {
	Describe("SimpleImageRefUpdater", func() {
		var updater *image_ref_updater.SimpleImageRefUpdater

		JustBeforeEach(func() {
			updater = image_ref_updater.NewSimpleUpdater(testing.NullLogger())
		})

		DescribeTable("updates correctly for",
			func(orgRef string, expectedNewRef string, expectedOK bool) {
				newRef, ok := updater.Update(orgRef)
				Expect(ok).To(Equal(expectedOK))
				Expect(newRef).To(Equal(expectedNewRef))
			},
			Entry("the helper image",
				"registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:ubi-fips-x86_64-latest",
				"registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper-dev:ubi-fips-x86_64-bleeding",
				true,
			),
			Entry("the runner image",
				"registry.gitlab.com/gitlab-org/gitlab-runner:ubi-fips",
				"registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-dev:ubi-fips-bleeding",
				true,
			),
			Entry("an unknown image",
				"dont-know-that-thang",
				"dont-know-that-thang",
				false,
			),
		)
	})
})
