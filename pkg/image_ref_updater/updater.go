package image_ref_updater

// The Simple Image Ref Update translates from an well-known, hardcoded
// "latest" image ref to a well-known, hardcoded "bleeding" image ref.
//
// This could be improved by implementing other Updaters which:
// - translate based on a config file
// - translate based on artifacts or logs of an CI run
//     potentially useful stuff for that:
//     - get the pipeline run for a MR:
//         op plugin run glab -- ci list -P 100 | grep "${MR_ID}"
//     - get the logs of a job of a pipeline run:
//         op plugin run -- glab ci trace -p "${PIPE_ID}" 'development docker images: [ubi-fips, amd64]'
//         op plugin run -- glab ci trace -p 1322615649 'development helper docker images: [ubi-fips, ubi-fips-]'

import "log/slog"

// SimpleImageRefUpdater updates the published image ref to the well-known
// "bleeding" refs. This is done for some well-known image refs. If called for
// an unknown image ref, it will produce a warning and return the original
// image ref.
type SimpleImageRefUpdater struct {
	logger *slog.Logger
}

func NewSimpleUpdater(logger *slog.Logger) *SimpleImageRefUpdater {
	return &SimpleImageRefUpdater{logger}
}

const baseRepo = "registry.gitlab.com/gitlab-org/gitlab-runner"

var inRepo = func(i string) string {
	return baseRepo + i
}

var refsMap = map[string]string{
	inRepo(":ubi-fips"): inRepo("/gitlab-runner-dev:ubi-fips-bleeding"),
	inRepo("/gitlab-runner-helper:ubi-fips-x86_64-latest"): inRepo("/gitlab-runner-helper-dev:ubi-fips-x86_64-bleeding"),
}

func (u *SimpleImageRefUpdater) Update(orgRef string) (string, bool) {
	logger := u.logger.With("orgRef", orgRef)

	if newRef, ok := refsMap[orgRef]; ok {
		logger.Info("image ref updated", "newRef", newRef)
		return newRef, true
	}

	logger.Warn("no update for image ref found, running with the original one")
	return orgRef, false
}
