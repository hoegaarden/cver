package image_ref_updater_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestImageRefUpdater(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "ImageRefUpdater Suite")
}
