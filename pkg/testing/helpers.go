package testing

import (
	"io"
	"log/slog"
)

func NullLogger() *slog.Logger {
	return slog.New(slog.NewTextHandler(io.Discard, nil))
}
