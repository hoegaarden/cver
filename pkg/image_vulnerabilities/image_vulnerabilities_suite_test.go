package image_vulnerabilities_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestImageVulnerabilities(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "ImageVulnerabilities Suite")
}
