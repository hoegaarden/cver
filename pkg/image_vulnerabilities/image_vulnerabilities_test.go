package image_vulnerabilities_test

import (
	"fmt"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/hoegaarden/cver/pkg/image_vulnerabilities"
	"gitlab.com/hoegaarden/cver/pkg/image_vulnerabilities/fakes"
	"gitlab.com/hoegaarden/cver/pkg/scan"
	"gitlab.com/hoegaarden/cver/pkg/testing"
)

var _ = Describe("ImageVulnerabilities", func() {
	var (
		issueLister     *fakes.FakeIssueLister
		imageScanner    *fakes.FakeImageScanner
		cveExtractor    *fakes.FakeCVEExtractor
		imageRefUpdater *fakes.FakeImageRefUpdater

		checker *image_vulnerabilities.Checker
	)

	BeforeEach(func() {
		issueLister = &fakes.FakeIssueLister{}
		imageScanner = &fakes.FakeImageScanner{}
		cveExtractor = &fakes.FakeCVEExtractor{}
		imageRefUpdater = &fakes.FakeImageRefUpdater{}
	})

	JustBeforeEach(func() {
		checker = &image_vulnerabilities.Checker{
			IssueLister:     issueLister,
			ImageScanner:    imageScanner,
			CVEExtractor:    cveExtractor,
			ImageRefUpdater: imageRefUpdater,
			Logger:          testing.NullLogger(),
		}
	})

	BeforeEach(func() {
		issueLister.IssuesReturns([]*gitlab.Issue{{Title: "some-issue-title"}}, nil)
		cveExtractor.ExtractReturns("CVE-123-123", "img:org", true)
		imageRefUpdater.UpdateReturns("img:gitRef", true)
		imageScanner.ScanReturns(scan.Vulnerabilies{"CVE-123-123": {}}, nil)
	})

	It("returns correct toKeep/toClose lists", func() {
		toClose, toKeep, err := checker.Run("someProject", "someLabelFilter", "someNotLabels")
		Expect(err).NotTo(HaveOccurred())

		Expect(issueLister.IssuesCallCount()).To(Equal(1))
		projectID, labelFilter, notLabels := issueLister.IssuesArgsForCall(0)
		Expect(projectID).To(Equal("someProject"))
		Expect(labelFilter).To(Equal("someLabelFilter"))
		Expect(notLabels).To(Equal("someNotLabels"))

		Expect(cveExtractor.ExtractCallCount()).To(Equal(1))
		Expect(cveExtractor.ExtractArgsForCall(0)).To(Equal("some-issue-title"))

		Expect(imageRefUpdater.UpdateCallCount()).To(Equal(1))
		Expect(imageRefUpdater.UpdateArgsForCall(0)).To(Equal("img:org"))

		Expect(imageScanner.ScanCallCount()).To(Equal(1))
		Expect(imageScanner.ScanArgsForCall(0)).To(Equal("img:gitRef"))

		Expect(toClose).To(BeEmpty())
		Expect(toKeep["img:gitRef"]["CVE-123-123"]).To(ContainElement(&gitlab.Issue{Title: "some-issue-title"}))
	})

	When("issue lister errors", func() {
		BeforeEach(func() {
			issueLister.IssuesReturns(nil, fmt.Errorf("some gathering error"))
		})
		It("returns early and bubbles up the error", func() {
			_, _, err := checker.Run("", "", "")
			Expect(err).To(MatchError(ContainSubstring("some gathering error")))

			Expect(issueLister.IssuesCallCount()).To(Equal(1))
			Expect(cveExtractor.ExtractCallCount()).To(Equal(0))
			Expect(imageRefUpdater.UpdateCallCount()).To(Equal(0))
			Expect(imageScanner.ScanCallCount()).To(Equal(0))
		})
	})

	When("cveExtractor cannot extract an image and CVE", func() {
		BeforeEach(func() {
			cveExtractor.ExtractReturns("cve-ignored", "img-ignored", false)
		})
		It("does no scans and returns empty lists", func() {
			toClose, toKeep, err := checker.Run("", "", "")
			Expect(err).NotTo(HaveOccurred())

			Expect(issueLister.IssuesCallCount()).To(Equal(1))
			Expect(cveExtractor.ExtractCallCount()).To(Equal(1))

			Expect(imageRefUpdater.UpdateCallCount()).To(Equal(0))
			Expect(imageScanner.ScanCallCount()).To(Equal(0))

			Expect(toKeep).To(BeEmpty())
			Expect(toClose).To(BeEmpty())
		})
	})

	When("imageRefUpdater does not find a image ref to update", func() {
		BeforeEach(func() {
			imageRefUpdater.UpdateReturns("this-is-ignored", false)
		})
		It("scans the original image", func() {
			toClose, toKeep, err := checker.Run("", "", "")
			Expect(err).NotTo(HaveOccurred())

			Expect(issueLister.IssuesCallCount()).To(Equal(1))
			Expect(cveExtractor.ExtractCallCount()).To(Equal(1))
			Expect(imageRefUpdater.UpdateCallCount()).To(Equal(1))

			Expect(imageScanner.ScanCallCount()).To(Equal(1))
			Expect(imageScanner.ScanArgsForCall(0)).To(Equal("img:org"))

			Expect(toKeep["img:org"]["CVE-123-123"]).To(ContainElement(&gitlab.Issue{Title: "some-issue-title"}))
			Expect(toClose).To(BeEmpty())
		})
	})

	Context("imageScanner", func() {
		When("it fails", func() {
			BeforeEach(func() {
				imageScanner.ScanReturns(nil, fmt.Errorf("some-scan-error"))
			})
			It("bubbles up the error", func() {
				_, _, err := checker.Run("", "", "")
				Expect(err).To(MatchError(ContainSubstring("some-scan-error")))
			})
		})

		When("it does not find any vulns", func() {
			BeforeEach(func() {
				imageScanner.ScanReturns(nil, nil)
			})
			It("suggest to close the issue", func() {
				toClose, toKeep, err := checker.Run("", "", "")
				Expect(err).NotTo(HaveOccurred())
				Expect(toKeep).To(BeEmpty())
				Expect(toClose["img:gitRef"]["CVE-123-123"]).To(ContainElement(&gitlab.Issue{Title: "some-issue-title"}))
			})
		})

		When("it finds vulns we don't have an issue for", func() {
			BeforeEach(func() {
				imageScanner.ScanReturns(scan.Vulnerabilies{
					"some-other-cve": {},
				}, nil)
			})
			It("ignores it, but suggests to close the other issue(s)", func() {
				toClose, toKeep, err := checker.Run("", "", "")
				Expect(err).NotTo(HaveOccurred())
				Expect(toKeep).To(BeEmpty())
				Expect(toClose["img:gitRef"]["CVE-123-123"]).To(ContainElement(&gitlab.Issue{Title: "some-issue-title"}))
			})
		})
	})

})
