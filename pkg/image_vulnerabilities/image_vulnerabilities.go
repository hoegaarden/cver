package image_vulnerabilities

import (
	"fmt"
	"log/slog"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/hoegaarden/cver/pkg/nested_bucket"
	"gitlab.com/hoegaarden/cver/pkg/scan"
)

type Checker struct {
	Logger          *slog.Logger
	IssueLister     IssueLister
	CVEExtractor    CVEExtractor
	ImageScanner    ImageScanner
	ImageRefUpdater ImageRefUpdater
}

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 -generate

//counterfeiter:generate -o fakes . IssueLister
type IssueLister interface {
	Issues(projectID string, labelFilter string, notLabels string) ([]*gitlab.Issue, error)
}

//counterfeiter:generate -o fakes . CVEExtractor
type CVEExtractor interface {
	Extract(input string) (cve, image string, ok bool)
}

//counterfeiter:generate -o fakes . ImageScanner
type ImageScanner interface {
	Scan(image string) (scan.Vulnerabilies, error)
}

//counterfeiter:generate -o fakes . ImageRefUpdater
type ImageRefUpdater interface {
	Update(input string) (output string, ok bool)
}

type IssueList = nested_bucket.Bucket[string, string, *gitlab.Issue]

func (c *Checker) Run(projectID, labelFilter string, notLabels string) (IssueList, IssueList, error) {
	issues, err := c.IssueLister.Issues(projectID, labelFilter, notLabels)
	if err != nil {
		return nil, nil, fmt.Errorf("gathering issues: %w", err)
	}

	reportedImageVulns := IssueList{}
	for _, issue := range issues {
		cveID, image, ok := c.CVEExtractor.Extract(issue.Title)
		if !ok {
			c.Logger.Info("ignoring issue", "url", issue.WebURL, "issue", issue.ID, "reason", "no CVE or image")
			continue
		}
		reportedImageVulns.Add(image, cveID, issue)
	}

	issuesToClose := IssueList{}
	issuesToKeep := IssueList{}

	for reportImage, reportedVulns := range reportedImageVulns {
		image := reportImage
		if c.ImageRefUpdater != nil {
			if newRef, ok := c.ImageRefUpdater.Update(reportImage); ok {
				image = newRef
			}
		}

		scannedVulns, err := c.ImageScanner.Scan(image)
		if err != nil {
			return nil, nil, fmt.Errorf("scanning image: %w", err)
		}

		for vuln, issues := range reportedVulns {
			_, isVulnerable := scannedVulns[vuln]
			if isVulnerable {
				c.Logger.Debug("image is vulnerable", "image", image, "vuln", vuln)
				issuesToKeep.Add(image, vuln, issues...)
			} else {
				c.Logger.Debug("image is NOT vulnerable", "image", image, "vuln", vuln)
				issuesToClose.Add(image, vuln, issues...)
			}
		}
	}

	return issuesToClose, issuesToKeep, nil
}
