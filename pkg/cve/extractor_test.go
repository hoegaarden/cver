package cve_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hoegaarden/cver/pkg/cve"
	helpers "gitlab.com/hoegaarden/cver/pkg/testing"
)

func TestCVE_REExtractor(t *testing.T) {
	testCases := map[string]struct {
		input           string
		expectedImage   string
		expectedCVE     string
		expectedFailure bool
	}{
		"happy": {
			input:         "CVE-2023-37920 detected in image registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:ubi-fips-x86_64-latest, package ca-certificates",
			expectedImage: "registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:ubi-fips-x86_64-latest",
			expectedCVE:   "CVE-2023-37920",
		},
		"does not match at all": {
			expectedFailure: true,
		},
		"no image": {
			input:           "CVE-asd-sdsd but no image",
			expectedFailure: true,
		},
		"no cve": {
			input:           "image suchandsuche",
			expectedFailure: true,
		},
	}

	for tn, tc := range testCases {
		reExtractor := cve.NewREExtractor(helpers.NullLogger())

		t.Run(tn, func(t *testing.T) {
			cve, img, ok := reExtractor.Extract(tc.input)

			if assert.Equal(t, tc.expectedFailure, !ok) {
				assert.Equal(t, tc.expectedImage, img)
				assert.Equal(t, tc.expectedCVE, cve)
			}
		})
	}
}
