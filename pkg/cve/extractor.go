package cve

import (
	"log/slog"
	"regexp"
)

func NewREExtractor(logger *slog.Logger) *REExtractor {
	return &REExtractor{
		logger: logger,
	}
}

type REExtractor struct {
	logger *slog.Logger
}

var extracCVEandImageRE = regexp.MustCompile(`(CVE-.*?)\s+.*image\s+(.*?)(?:,|\s|$)`)

func (e *REExtractor) Extract(input string) (string, string, bool) {
	matches := extracCVEandImageRE.FindStringSubmatch(input)
	if len(matches) != 3 {
		e.logger.Debug("no CVE or image ref found", "input", input, "RE", extracCVEandImageRE.String())
		return "", "", false
	}

	// match[0] is the whole input string
	cve, img := matches[1], matches[2]
	e.logger.Debug("CVE and image ref found", "image", img, "CVE", cve)
	return matches[1], matches[2], true
}
