package scan_test

import (
	"fmt"
	"io"
	"strconv"
	"strings"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/hoegaarden/cver/pkg/scan"
	"gitlab.com/hoegaarden/cver/pkg/scan/fakes"
	"gitlab.com/hoegaarden/cver/pkg/scan/testdata"
	"gitlab.com/hoegaarden/cver/pkg/testing"
)

var _ = Describe("Scan", func() {
	var (
		commandCreator *fakes.FakeCommandCreator
		command        *fakes.FakeCommand

		grype *scan.Grype
	)

	BeforeEach(func() {
		commandCreator = &fakes.FakeCommandCreator{}
		command = &fakes.FakeCommand{}

		commandCreator.Returns(command)
	})

	JustBeforeEach(func() {
		grype = scan.NewGrypeScanner(testing.NullLogger())
		grype.CommandCreator = commandCreator.Spy
	})

	It("creates and runs the command", func() {
		_, err := grype.Scan("some-image-ref")
		Expect(err).NotTo(HaveOccurred())
		Expect(commandCreator.CallCount()).To(Equal(1))

		cmd, stdout, stderr, args := commandCreator.ArgsForCall(0)
		Expect(cmd).To(Equal("grype"))
		Expect(stdout).NotTo(BeNil())
		Expect(stderr).NotTo(BeNil())
		Expect(args).To(Equal(
			[]string{"--output", "json", "--quiet", "some-image-ref"},
		))
	})

	When("the command returns an error", func() {
		BeforeEach(func() {
			command.RunReturns(fmt.Errorf("some-runtime-error"))
		})
		It("bubbles up the error", func() {
			_, err := grype.Scan("some-image-ref")
			Expect(err).To(MatchError(And(
				ContainSubstring("running scanner"),
				ContainSubstring("some-runtime-error"),
			)))
		})
	})

	When("the scanner returns garbage", func() {
		BeforeEach(func() {
			commandCreator.Calls(func(cmd string, stdout, stderr io.Writer, args ...string) scan.Command {
				badJson := "{ this is clearly not valid json"
				s, err := fmt.Fprintf(stdout, "%s", badJson)
				Expect(err).NotTo(HaveOccurred())
				Expect(s).To(Equal(len(badJson)))
				return command
			})
		})
		It("errors", func() {
			_, err := grype.Scan("some-image-ref")
			Expect(err).To(MatchError(And(
				ContainSubstring("invalid"),
				ContainSubstring("deserializing results"),
			)))
		})
	})

	When("the command returns proper output", func() {
		DescribeTable("is able to process scanner output for",
			func(rawScanResult string, expectedVulnsStr string) {
				commandCreator.Calls(func(cmd string, stdout, stderr io.Writer, args ...string) scan.Command {
					s, err := fmt.Fprintf(stdout, "%s", rawScanResult)
					Expect(err).NotTo(HaveOccurred())
					Expect(s).To(Equal(len(rawScanResult)))
					return command
				})

				vulns, err := grype.Scan("unused")
				Expect(err).NotTo(HaveOccurred())

				i, err := strconv.Atoi(strings.TrimSpace(expectedVulnsStr))
				Expect(err).NotTo(HaveOccurred())
				Expect(len(vulns)).To(Equal(i))
			},
			Entry("debian", testdata.DebianScanResult, testdata.DebianVulnCount),
			Entry("distroless", testdata.DistrolessScanResult, testdata.DistrolessVulnCount),
		)
	})
})

var _ = Describe("default CommandCreator", func() {
	It("creates a real *exec.Cmd", func() {
		scanner := scan.NewGrypeScanner(testing.NullLogger())
		cmd := scanner.CommandCreator.Create("", nil, nil)
		Expect(fmt.Sprintf("%T", cmd)).To(Equal("*exec.Cmd"))
	})
})
