package scan

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"os/exec"
)

func NewGrypeScanner(logger *slog.Logger) *Grype {
	return &Grype{logger: logger}
}

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 -generate

//counterfeiter:generate -o fakes . Command
type Command interface {
	Run() error
}

//counterfeiter:generate -o fakes . CommandCreator
type CommandCreator func(cmd string, stdout, stderr io.Writer, args ...string) Command

func (c CommandCreator) Create(cmd string, stdout, stderr io.Writer, args ...string) Command {
	if c == nil {
		c = defaultCommandCreator
	}
	return c(cmd, stdout, stderr, args...)
}

var defaultCommandCreator = func(cmd string, stdout, stderr io.Writer, args ...string) Command {
	c := exec.Command(cmd, args...)
	c.Stdout = stdout
	c.Stderr = stderr
	return c
}

type Grype struct {
	CommandCreator CommandCreator
	logger         *slog.Logger
}

type Vulnerabilies map[string]Vulnerability
type Vulnerability struct {
	Severity        string
	ArtifactName    string
	ArtifactType    string
	ArtifactVersion string
}

func (g *Grype) Scan(image string) (Vulnerabilies, error) {
	logger := g.logger.With("image", image)

	stdout, stderr := &bytes.Buffer{}, &bytes.Buffer{}

	cmd := g.CommandCreator.Create("grype", stdout, stderr, "--output", "json", "--quiet", image)

	logger.Debug("scanning")
	if err := cmd.Run(); err != nil {
		logger.Error("running scanner", "err", err, "stdout", stdout.String(), "stderr", stderr.String())
		return nil, fmt.Errorf("running scanner: %w", err)
	}

	findings := scanFindings{}
	if err := json.NewDecoder(stdout).Decode(&findings); err != nil && err != io.EOF {
		logger.Error("deserializing result", "err", err)
		return nil, fmt.Errorf("deserializing results: %w", err)
	}

	vulns := Vulnerabilies{}
	for _, match := range findings.Matches {
		vulns[match.Vulnerability.ID] = Vulnerability{
			Severity:        match.Vulnerability.Severity,
			ArtifactName:    match.Artifact.Name,
			ArtifactVersion: match.Artifact.Version,
			ArtifactType:    match.Artifact.Type,
		}
	}

	logger.Debug("found vulnerabilies", "vulns", vulns)

	return vulns, nil
}

// scanFindings is to pull out a subset of the scan results
type scanFindings struct {
	Matches []struct {
		Vulnerability struct {
			ID       string `json:"id"`
			Severity string `json:"severity"`
		} `json:"vulnerability"`
		Artifact struct {
			Name    string `json:"name"`
			Version string `json:"version"`
			Type    string `json:"type"`
		} `json:"artifact"`
	} `json:"matches"`
}
