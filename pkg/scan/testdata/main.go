package testdata

// Note: you need to run `go generate` explicitly in the testdata package, it
// won't run in testdata by default

import _ "embed"

//go:generate echo updating distroless test data
//go:generate grype --output json --quiet --file distroless.json  gcr.io/distroless/base-debian12
//go:embed distroless.json
var DistrolessScanResult string

//go:generate $SHELL -c "jq '[ .matches[].vulnerability.id ] | unique | length' distroless.json > distroless_vulns.json"
//go:embed distroless_vulns.json
var DistrolessVulnCount string

//go:generate echo updating debian test data
//go:generate grype --output json --quiet --file debian.json debian:latest
//go:embed debian.json
var DebianScanResult string

//go:generate $SHELL -c "jq '[ .matches[].vulnerability.id ] | unique | length' debian.json > debian_vulns.json"
//go:embed debian_vulns.json
var DebianVulnCount string
