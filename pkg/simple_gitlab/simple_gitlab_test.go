package simple_gitlab_test

import (
	"fmt"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/hoegaarden/cver/pkg/simple_gitlab"
	"gitlab.com/hoegaarden/cver/pkg/simple_gitlab/fakes"
)

var _ = Describe("SimpleGitlab", func() {
	var (
		issueService *fakes.FakeIssueService
		noteService  *fakes.FakeNoteService

		client *simple_gitlab.API
	)

	BeforeEach(func() {
		issueService = &fakes.FakeIssueService{}
		noteService = &fakes.FakeNoteService{}
	})

	JustBeforeEach(func() {
		client = &simple_gitlab.API{
			IssueService: issueService,
			NoteService:  noteService,
		}
	})

	Describe("Issues", func() {
		BeforeEach(func() {
			issueService.ListProjectIssuesReturns([]*gitlab.Issue{
				{ID: 1234, Title: "some-title"},
				{ID: 1235, Title: "some-other-title"},
			}, &gitlab.Response{}, nil)
		})

		It("gets some issues", func() {
			issues, err := client.Issues("some-project", "some-label-filter", "some-not-labels")
			Expect(err).NotTo(HaveOccurred())

			Expect(issues).To(HaveLen(2))
			Expect(issueService.ListProjectIssuesCallCount()).To(Equal(1))

			project, opts, _ := issueService.ListProjectIssuesArgsForCall(0)
			Expect(project).To(Equal("some-project"))
			Expect(*opts.Labels).To(ContainElement("some-label-filter"))
			Expect(opts.ListOptions.Page).To(Equal(0))
			Expect(opts.ListOptions.PerPage).To(BeNumerically(">", 0))
			Expect(*opts.Confidential).To(BeTrue())
			Expect(*opts.State).To(Equal("opened"))
			Expect(*opts.NotLabels).To(ContainElement("some-not-labels"))
		})

		When("no not-labels are provided", func() {
			It("does not send not-labels to the API", func() {
				_, _ = client.Issues("some-project", "some-label-filter", "")
				_, opts, _ := issueService.ListProjectIssuesArgsForCall(0)
				Expect(opts.NotLabels).To(BeNil())
			})
		})

		When("GitLab returns an error", func() {
			BeforeEach(func() {
				issueService.ListProjectIssuesReturns(nil, nil, fmt.Errorf("some-api-error"))
			})

			It("bubbles up the error", func() {
				_, err := client.Issues("", "", "")
				Expect(err).To(MatchError("some-api-error"))
			})
		})

		When("we see multiple pages", func() {
			BeforeEach(func() {
				issueService.ListProjectIssuesReturnsOnCall(0, nil, &gitlab.Response{NextPage: 1}, nil)
				issueService.ListProjectIssuesReturnsOnCall(1, nil, &gitlab.Response{NextPage: 32}, nil)
				issueService.ListProjectIssuesReturnsOnCall(2, nil, &gitlab.Response{NextPage: 0}, nil)
			})

			It("collects all pages until we get `NextPage == 0`", func() {
				_, err := client.Issues("", "", "")
				Expect(err).NotTo(HaveOccurred())

				Expect(issueService.ListProjectIssuesCallCount()).To(Equal(3))

				_, opts0, _ := issueService.ListProjectIssuesArgsForCall(0)
				_, opts1, _ := issueService.ListProjectIssuesArgsForCall(1)
				_, opts2, _ := issueService.ListProjectIssuesArgsForCall(2)
				Expect(opts0.ListOptions.Page).To(Equal(0))
				Expect(opts1.ListOptions.Page).To(Equal(1))
				Expect(opts2.ListOptions.Page).To(Equal(32))
			})

			When("an error occurs on a later page", func() {
				BeforeEach(func() {
					issueService.ListProjectIssuesReturnsOnCall(1, nil, &gitlab.Response{NextPage: 32}, fmt.Errorf("error-on-2nd-page"))
				})
				It("aborts and bubbles up the error", func() {
					issues, err := client.Issues("", "", "")
					Expect(err).To(MatchError("error-on-2nd-page"))
					Expect(issues).To(BeNil())

					Expect(issueService.ListProjectIssuesCallCount()).To(Equal(2))
				})
			})
		})
	})

	Describe("CloseIssue", func() {
		It("closes the issue with a comment", func() {
			err := client.CloseIssue("some-project", 1234, "closing comment")
			Expect(err).NotTo(HaveOccurred())

			By("checking notes interactions", func() {
				Expect(noteService.CreateIssueNoteCallCount()).To(Equal(1))

				project, issueID, opts, _ := noteService.CreateIssueNoteArgsForCall(0)
				Expect(project).To(Equal("some-project"))
				Expect(issueID).To(Equal(1234))
				Expect(*opts.Body).To(Equal("closing comment"))
			})

			By("checking issues interactions", func() {
				Expect(issueService.UpdateIssueCallCount()).To(Equal(1))

				project, issueID, opts, _ := issueService.UpdateIssueArgsForCall(0)
				Expect(project).To(Equal("some-project"))
				Expect(issueID).To(Equal(1234))
				Expect(*opts.StateEvent).To(Equal("close"))
			})
		})

		When("adding the comment errors", func() {
			BeforeEach(func() {
				noteService.CreateIssueNoteReturns(nil, nil, fmt.Errorf("error-on-note"))
			})
			It("bubbles up the error and does not close", func() {
				err := client.CloseIssue("", 0, "some-comment")
				Expect(err).To(MatchError("error-on-note"))
				Expect(noteService.CreateIssueNoteCallCount()).To(Equal(1))
				Expect(issueService.UpdateIssueCallCount()).To(Equal(0))
			})
		})

		When("issue update errors", func() {
			BeforeEach(func() {
				issueService.UpdateIssueReturns(nil, nil, fmt.Errorf("error-on-update"))
			})
			It("returns the error", func() {
				err := client.CloseIssue("", 0, "some-comment")
				Expect(err).To(MatchError("error-on-update"))
				Expect(noteService.CreateIssueNoteCallCount()).To(Equal(1))
				Expect(issueService.UpdateIssueCallCount()).To(Equal(1))
			})
		})

		When("no note is added", func() {
			It("only closes and does not add a note", func() {
				err := client.CloseIssue("", 0, "")
				Expect(err).NotTo(HaveOccurred())
				Expect(noteService.CreateIssueNoteCallCount()).To(Equal(0))
				Expect(issueService.UpdateIssueCallCount()).To(Equal(1))
			})

			When("issue update errors", func() {
				BeforeEach(func() {
					issueService.UpdateIssueReturns(nil, nil, fmt.Errorf("error-on-update"))
				})
				It("returns the error", func() {
					err := client.CloseIssue("", 0, "")
					Expect(err).To(MatchError("error-on-update"))
				})
			})
		})

	})
})
