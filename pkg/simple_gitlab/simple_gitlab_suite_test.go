package simple_gitlab_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestSimpleGitlab(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "SimpleGitlab Suite")
}
