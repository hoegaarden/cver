package simple_gitlab

import (
	"log/slog"

	"github.com/xanzy/go-gitlab"
)

const (
	defaultPageItems = 100
)

var (
	eventClose = gitlab.Ptr("close")
	stateOpen  = gitlab.Ptr("opened")
)

func NewAPI(token string, logger *slog.Logger) (*API, error) {
	client, err := gitlab.NewClient(token, gitlab.WithCustomLeveledLogger(logger))
	if err != nil {
		return nil, err
	}

	return &API{
		IssueService: client.Issues,
		NoteService:  client.Notes,

		logger: logger,
	}, nil
}

type API struct {
	IssueService IssueService
	NoteService  NoteService

	logger *slog.Logger
}

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 -generate

//counterfeiter:generate -o fakes . IssueService
type IssueService interface {
	ListProjectIssues(project any, opts *gitlab.ListProjectIssuesOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Issue, *gitlab.Response, error)
	UpdateIssue(project any, issueID int, opts *gitlab.UpdateIssueOptions, options ...gitlab.RequestOptionFunc) (*gitlab.Issue, *gitlab.Response, error)
}

//counterfeiter:generate -o fakes . NoteService
type NoteService interface {
	CreateIssueNote(project any, issueID int, opts *gitlab.CreateIssueNoteOptions, options ...gitlab.RequestOptionFunc) (*gitlab.Note, *gitlab.Response, error)
}

func (api *API) Issues(project string, labelFilter string, notLabels string) ([]*gitlab.Issue, error) {
	opts := gitlab.ListProjectIssuesOptions{
		Confidential: gitlab.Ptr(true),
		Labels:       &gitlab.LabelOptions{labelFilter},
		State:        stateOpen,
		ListOptions: gitlab.ListOptions{
			Page:    0,
			PerPage: defaultPageItems,
		},
	}
	if notLabels != "" {
		opts.NotLabels = &gitlab.LabelOptions{notLabels}
	}

	results := []*gitlab.Issue{}

	for {
		issues, resp, err := api.IssueService.ListProjectIssues(project, gitlab.Ptr(opts))
		if err != nil {
			return nil, err
		}

		results = append(results, issues...)
		opts.ListOptions.Page = resp.NextPage

		if resp.NextPage == 0 {
			break
		}
	}

	return results, nil
}

func (api *API) CloseIssue(project string, issueID int, closeComment string) error {
	if closeComment != "" {
		_, _, err := api.NoteService.CreateIssueNote(project, issueID, &gitlab.CreateIssueNoteOptions{
			Body: gitlab.Ptr(closeComment),
		})
		if err != nil {
			return err
		}
	}

	_, _, err := api.IssueService.UpdateIssue(project, issueID, &gitlab.UpdateIssueOptions{
		StateEvent: eventClose,
	})

	return err
}
