package cmd

import (
	"fmt"
	"log/slog"

	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/hoegaarden/cver/pkg/cve"
	"gitlab.com/hoegaarden/cver/pkg/image_ref_updater"
	"gitlab.com/hoegaarden/cver/pkg/image_vulnerabilities"
	"gitlab.com/hoegaarden/cver/pkg/nested_bucket"
	"gitlab.com/hoegaarden/cver/pkg/scan"
	"gitlab.com/hoegaarden/cver/pkg/simple_gitlab"
	"gopkg.in/yaml.v3"
)

const (
	flagNameProjectID   = "projectID"
	flagNameLabelFilter = "labelFilter"
	flageNameNotLabels  = "notLabels"
	flagNameAutoClose   = "autoClose"
	flagNameUnattended  = "unattended"
	flagNamePublished   = "published"

	defaultProjectID   = "250833" // gitlab-org/gitlab-runner
	defaultLabelFilter = "FedRAMP::Vulnerability"
	defaultNotLabels   = "FedRAMP::DR Status::Open"
	defaultAutoClose   = false
	defaultUnattended  = false
	defaultPublished   = false
)

func imageVulnCmd() *cobra.Command {
	var (
		flagProjectID   string
		flagLabelFilter string
		flagNotLabels   string
		flagAutoClose   bool
		flagUnattended  bool
		flagPublished   bool
	)

	cmd := &cobra.Command{
		Use:   "imageVulns",
		Short: "Get image vulnerability issues and check if the newest builds of the images are still vulnerable",
		RunE: func(cmd *cobra.Command, args []string) error {
			stdout := cmd.OutOrStdout()
			logger := slog.With("command", cmd.Name())

			// To not leak the var in shell history or such, we only read the token from the env
			gitlabToken, err := gitlabToken()
			if err != nil {
				return err
			}
			gitlabAPI, err := simple_gitlab.NewAPI(gitlabToken, logger.With("component", "GitlabClient"))
			if err != nil {
				return err
			}

			checker := &image_vulnerabilities.Checker{
				Logger:       logger,
				IssueLister:  gitlabAPI,
				CVEExtractor: cve.NewREExtractor(logger.With("component", "CVEImageExtractor")),
				ImageScanner: scan.NewGrypeScanner(logger.With("component", "ImageScanner")),
			}
			if !flagPublished {
				checker.ImageRefUpdater = image_ref_updater.NewSimpleUpdater(logger.With("component", "ImageRefUpdater"))
			}

			issuesToClose, issuesToKeep, err := checker.Run(flagProjectID, flagLabelFilter, flagNotLabels)
			if err != nil {
				return err
			}

			{ // TODO: printing / formatting output should be pulled out
				{
					bytes, err := yaml.Marshal(nested_bucket.Map(issuesToClose, func(issue *gitlab.Issue) string {
						return issue.WebURL
					}))
					if err != nil {
						return fmt.Errorf("marshalling result: %w", err)
					}

					fmt.Fprintf(stdout, "Close these issues, because CVE not found in image:\n---\n%s...\n", bytes)
				}

				fmt.Fprintln(stdout)

				{
					bytes, err := yaml.Marshal(nested_bucket.Map(issuesToKeep, func(issue *gitlab.Issue) string {
						return issue.WebURL
					}))
					if err != nil {
						return fmt.Errorf("marshalling result: %w", err)
					}

					fmt.Fprintf(stdout, "Keep these issues, because CVE still found in image:\n---\n%s...\n", bytes)
				}
			}

			if flagAutoClose { // TODO: externalize
				logger := logger.With("section", "autoClose", "unattended", flagUnattended)
				confirmer := &Confirmer{In: cmd.InOrStdin(), Out: stdout, Unattended: flagUnattended}

				err := nested_bucket.Each(issuesToClose, func(image, cve string, issue *gitlab.Issue, idx int) error {
					return confirmer.AskOrElse(
						fmt.Sprintf("close issue %s (img: %s, CVE: %s)", issue.WebURL, image, cve),
						func() error { // when user says yep
							logger.Debug("closing issue", "issue", issue.WebURL)
							return gitlabAPI.CloseIssue(flagProjectID, issue.IID, fmt.Sprintf("[%s] closing %s, was not found in `%s` anymore", cmd.Root().Name(), cve, image))
						},
						func() error { // when user says nope
							logger.Debug("not closing issue", "issue", issue.WebURL, "reason", "user-not-confirmed")
							return nil
						},
					)
				})
				if err != nil {
					return err
				}
			}

			return nil
		},
	}

	cmd.Flags().StringVarP(&flagProjectID, flagNameProjectID, "p", defaultProjectID, "GitLab project ID")
	cmd.Flags().StringVarP(&flagLabelFilter, flagNameLabelFilter, "", defaultLabelFilter, "Label Filter for GitLab issues")
	cmd.Flags().StringVarP(&flagNotLabels, flageNameNotLabels, "", defaultNotLabels, "exclude issue with these labels")
	cmd.Flags().BoolVarP(&flagAutoClose, flagNameAutoClose, "", defaultAutoClose, "Automatically close issue that are not relevant anymore")
	cmd.Flags().BoolVarP(&flagUnattended, flagNameUnattended, "", defaultUnattended, "Don't ask user for input")
	cmd.Flags().BoolVarP(&flagPublished, flagNamePublished, "", defaultPublished, "Run scan against the published images, and not the latest (unpublished) builds")

	return cmd
}
