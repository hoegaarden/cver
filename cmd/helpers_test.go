package cmd_test

import (
	"bytes"
	"fmt"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/hoegaarden/cver/cmd"
)

var _ = Describe("Helpers", func() {

	Describe("Confirmer", func() {
		var (
			stdout *bytes.Buffer
			stdin  *bytes.Buffer

			unattended bool

			confirmer     *cmd.Confirmer
			confirmAction *fakeAction
		)

		BeforeEach(func() {
			stdout = bytes.NewBuffer([]byte{})
			stdin = bytes.NewBuffer([]byte{})

			confirmAction = &fakeAction{}

			unattended = false
		})

		JustBeforeEach(func() {
			confirmer = &cmd.Confirmer{
				In:         stdin,
				Out:        stdout,
				Unattended: unattended,
			}
		})

		It("runs the action", func() {
			stdin.WriteString("y")

			err := confirmer.Ask("some-question", confirmAction.Fn())

			Expect(err).NotTo(HaveOccurred())
			Expect(confirmAction.Ran()).To(BeTrue())
			Expect(stdout.String()).To(Equal("some-question? "))
		})

		When("the confirmation action returns an error", func() {
			BeforeEach(func() {
				confirmAction.returns = fmt.Errorf("some-error")
			})
			It("returns that error", func() {
				stdin.WriteString("YeS")

				err := confirmer.Ask("some-question", confirmAction.Fn())

				Expect(err).To(MatchError("some-error"))
				Expect(confirmAction.Ran()).To(BeTrue())
				Expect(stdout.String()).To(Equal("some-question? "))
			})

			When("but there was no confirmation", func() {
				It("does not run the confirmAction and therefor does not error", func() {
					err := confirmer.Ask("some-question", confirmAction.Fn())

					Expect(err).NotTo(HaveOccurred())
					Expect(confirmAction.Ran()).To(BeFalse())
					Expect(stdout.String()).To(Equal("some-question? "))
				})
			})
		})

		When("it's running in unattended mode", func() {
			BeforeEach(func() {
				unattended = true
			})
			It("runs the confirmation action right away and does nothing else", func() {
				stdin.WriteString("will-not-be-consumed")
				err := confirmer.Ask("some-question", confirmAction.Fn())

				Expect(err).NotTo(HaveOccurred())
				Expect(stdout.String()).To(BeEmpty())
				Expect(confirmAction.Ran()).To(BeTrue())
				// because stdin has not been consumed by the confirmer
				Expect(stdin.String()).To(Equal("will-not-be-consumed"))
			})
		})

		Context("with an 'else branch'", func() {
			var (
				notConfirmAction *fakeAction
			)
			BeforeEach(func() {
				notConfirmAction = &fakeAction{}
			})

			It("runs the 'else branch' when not confirmed", func() {
				err := confirmer.AskOrElse("q", confirmAction.Fn(), notConfirmAction.Fn())

				Expect(err).NotTo(HaveOccurred())
				Expect(confirmAction.Ran()).To(BeFalse())
				Expect(notConfirmAction.Ran()).To(BeTrue())
			})

			When("the 'else branch' returns an error", func() {
				BeforeEach(func() {
					notConfirmAction.returns = fmt.Errorf("oh-noo")
				})
				It("bubbles up the error", func() {
					err := confirmer.AskOrElse("q", confirmAction.Fn(), notConfirmAction.Fn())

					Expect(err).To(MatchError("oh-noo"))
				})
			})
		})

		Context("expected response", func() {
			JustBeforeEach(func() {
				confirmer.ResponseCheck = func(resp string) bool {
					return resp == "please"
				}
			})
			It("does not run the confirmation action on 'y'", func() {
				stdin.WriteString("y")
				err := confirmer.Ask("", confirmAction.Fn())
				Expect(err).NotTo(HaveOccurred())
				Expect(confirmAction.Ran()).To(BeFalse())
			})
			It("does run the confirmation action on the expected response", func() {
				stdin.WriteString("please")
				err := confirmer.Ask("", confirmAction.Fn())
				Expect(err).NotTo(HaveOccurred())
				Expect(confirmAction.Ran()).To(BeTrue())
			})
		})
	})
})

type fakeAction struct {
	returns   error
	callCount int
}

func (c *fakeAction) Fn() func() error {
	return func() error {
		c.callCount += 1
		return c.returns
	}
}

func (c *fakeAction) Ran() bool {
	return c.callCount == 1
}
