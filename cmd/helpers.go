package cmd

import (
	"fmt"
	"io"
	"os"
	"strings"
)

const (
	gitlabTokenEnvVarName = "GITLAB_TOKEN"
)

func envVarGetter(name string) func() (string, error) {
	return func() (string, error) {
		val, ok := os.LookupEnv(name)
		if !ok {
			return "", fmt.Errorf("environment variable %s not set", name)
		}
		return val, nil
	}
}

var gitlabToken = envVarGetter(gitlabTokenEnvVarName)

type Confirmer struct {
	In            io.Reader
	Out           io.Writer
	Unattended    bool
	ResponseCheck func(resp string) bool
}

type confirmerAction = func() error

func (c *Confirmer) Ask(question string, onConfirmed confirmerAction) error {
	return c.AskOrElse(question, onConfirmed, nil)
}

func (c *Confirmer) AskOrElse(question string, onConfirmed, onNotConfirmed confirmerAction) error {
	if c.Unattended {
		return onConfirmed()
	}

	_, err := fmt.Fprintf(c.Out, "%s? ", question)
	if err != nil {
		return err
	}

	var resp string
	_, err = fmt.Fscanln(c.In, &resp)
	if err != nil && err != io.EOF {
		return err
	}

	if c.ResponseCheck == nil {
		c.ResponseCheck = func(resp string) bool {
			lc := strings.ToLower(resp)
			return lc == "y" || lc == "yes"
		}
	}

	if confirmed := c.ResponseCheck(resp); confirmed {
		return onConfirmed()
	}

	if onNotConfirmed != nil {
		return onNotConfirmed()
	}

	return nil
}
