package cmd

import (
	"log/slog"
	"os"

	"github.com/spf13/cobra"
)

const (
	flagNameLogLevel = "logLevel"
)

func rootCmd() *cobra.Command {
	var (
		flagLogLevel string
	)

	cmd := &cobra.Command{
		Use: "cver",
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			level := slog.Level(0)
			if err := level.UnmarshalText([]byte(flagLogLevel)); err != nil {
				return err
			}

			slog.SetDefault(slog.New(slog.NewTextHandler(cmd.ErrOrStderr(), &slog.HandlerOptions{
				Level: level,
			})))

			return nil
		},
	}

	cmd.PersistentFlags().StringVarP(&flagLogLevel, flagNameLogLevel, "", slog.LevelInfo.String(), "Log level")

	return cmd
}

func Execute() {
	rootCmd := rootCmd()

	rootCmd.AddCommand(imageVulnCmd())

	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
