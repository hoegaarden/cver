package main

import (
	"gitlab.com/hoegaarden/cver/cmd"
)

func main() {
	cmd.Execute()
}
